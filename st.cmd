require fbis
require essioc


iocshLoad("$(fbis_DIR)MPSID.iocsh")
iocshLoad("$(fbis_DIR)MPSMag.iocsh")
iocshLoad("$(fbis_DIR)MPSVac.iocsh")
iocshLoad("$(fbis_DIR)MPSTrg.iocsh")
iocshLoad("$(fbis_DIR)FIST.iocsh")
iocshLoad("$(fbis_DIR)SIS.iocsh")
iocshLoad("$(fbis_DIR)BCM.iocsh")
iocshLoad("$(fbis_DIR)BPM.iocsh")
iocshLoad("$(fbis_DIR)BLM.iocsh")
iocshLoad("$(fbis_DIR)IMG.iocsh")
iocshLoad("$(fbis_DIR)GRD.iocsh")
iocshLoad("$(fbis_DIR)APT.iocsh")
iocshLoad("$(fbis_DIR)IS_PLC.iocsh")
iocshLoad("$(fbis_DIR)LEBT_Ch.iocsh")
iocshLoad("$(fbis_DIR)MEBT_Ch.iocsh")
iocshLoad("$(fbis_DIR)MEBT_COLL.iocsh")
iocshLoad("$(fbis_DIR)RF.iocsh")
iocshLoad("$(fbis_DIR)VVF.iocsh")
iocshLoad("$(fbis_DIR)PSS.iocsh")
iocshLoad("$(fbis_DIR)TSS.iocsh")
iocshLoad("$(fbis_DIR)mpsos.iocsh")
iocshLoad("$(fbis_DIR)systems_mpsos.iocsh")

##-HW status
dbLoadRecords("hw_dln_status.db", "P=FBIS-DLN01:")
dbLoadRecords("hw_dln_status.db", "P=FBIS-DLN02:")
dbLoadRecords("hw_dln_status.db", "P=FBIS-DLN03:")
dbLoadRecords("hw_dln_status.db", "P=FBIS-DLN04:")
dbLoadRecords("hw_dln_status.db", "P=FBIS-DLN05:")
dbLoadRecords("hw_dln_status.db", "P=FBIS-DLN06:")
dbLoadRecords("hw_dln_status.db", "P=FBIS-DLN07:")

dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU01:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU02:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU03:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU04:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU05:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU06:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU07:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU08:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU09:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU10:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU11:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU12:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU13:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU14:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU16:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU18:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU20:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU22:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU23:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU24:")
dbLoadRecords("hw_scu_status.db", "P=FBIS-SCU25:")


iocshLoad("$(essioc_DIR)/common_config.iocsh")

callbackSetQueueSize(20000)
scanOnceSetQueueSize(20000)

iocInit

#- Shifted Beam Destination PV mbbi index
dbpf FBIS::#SIS-PBD-CO-SP.CALC '((A>=0) AND (A<=8))?A+3:1'
